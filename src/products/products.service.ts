import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'Hot Chocolate', price: 65 },
  { id: 2, name: 'Ice Espresso', price: 75 },
  { id: 3, name: 'Thai Tea', price: 65 },
];
let lastProductId = 4;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'Hot Chocolate', price: 65 },
      { id: 2, name: 'Ice Espresso', price: 75 },
      { id: 3, name: 'Thai Tea', price: 65 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
